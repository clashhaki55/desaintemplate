(function ($){
    $('.slider-vegas').vegas({
      transition: ['fade','burn','zoomIn'],
      slides: [
        { src: "slider/asal.jpg", transition: 'slideRight' },
        { src: "slider/tes.jpg", transition: 'swirlRight2' },
        { src: "slider/pantai.jpg" }
      ]
    });

})(jQuery);
